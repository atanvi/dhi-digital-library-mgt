package com.heraizen.dhi.dl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalLibrarySvc{

	public static void main(String[] args) {
		SpringApplication.run(DigitalLibrarySvc.class, args);
	}


}
